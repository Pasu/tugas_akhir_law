$(function() {
    $('#upload-btn').click(function() {
        var $form = $("#form-url");
        var data = getFormData($form);
        var routing_key = makeUniqueid(10);
        var listUrl = window.location.href.split("?");
        data["routing_key"] = routing_key;
        console.log(data)
        $.ajax({
            type: 'POST',
            headers: {"X-ROUTING-KEY": routing_key},
            url: listUrl[0],
            contentType: false,
            cache: false,
            processData: false,
            success: function(response) {
                console.log(response.status)
                WebSocket(routing_key,data)
            },
            error: function(response) {
                console.log(response)
            }
        });
    });
    $('#start-button').click(function() {
        var x = document.getElementsByClassName("form-download");
        x[0].removeAttribute("style")
        var y = document.getElementsByClassName("progress-compress");
        y[0].style.display = "none"
        var z = document.getElementById("start-button");
        z.style.display = "none";
        xx = document.getElementsByClassName("download-progress-bar")
        console.log(xx)
        for(let i=0;i<10;i++){
            xx[i].innerHTML = "0%"
            xx[i].style.width = "0%"
        }
        yy = document.getElementById("compress-progress-bar");
        yy[0].style.width = "0%"
        yy[0].innerHTML = "0%"
        for (let i=1;i<11;i++){
            formName = '#url-'+i;
            $(formName).val("");
        }
        document.getElementById("time-label").innerHTML = "";
    });
});

function progressDownloadPage(){
    var x = document.getElementsByClassName("form-download");
    x[0].style.display = "none";
    document.getElementById("status").innerHTML = ""
    var y = document.getElementsByClassName("progress-download");
    y[0].removeAttribute("style")
}

function makeUniqueid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }



//sumber
// https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
// http://jmesnil.net/stomp-websocket/doc/
async function WebSocket(routing_key,form_data){
    if ("WebSocket" in window) {
        await sleep(1000); // 1 detik
        progressDownloadPage();
        var time_key = routing_key+".time.key"
        var compressing_key = routing_key+".compressing.key"
        var direct_key = routing_key+".direct.key"
        routing_key = routing_key+".download.key"
        var ws_stomp_display = new SockJS( 'http://152.118.148.95:15674/stomp');
        var client_display = Stomp.over(ws_stomp_display);

        var mq_queue_display = "/exchange/1606896174_DIRECT/" + direct_key;
        var mq_queue_display_for_download = "/exchange/1606896174_TOPIC/" + routing_key;
        var mq_queue_display_for_time = "/exchange/1606896174_TOPIC/" + time_key;
        var mq_queue_display_for_compressing = "/exchange/1606896174_TOPIC/" + compressing_key;

        var on_connect_display = function() {
            client_display.subscribe(mq_queue_display,on_message_display)
            console.log('connected');
            client_display.subscribe(mq_queue_display_for_download, on_message_display_for_download);
            console.log('connected');
            client_display.subscribe(mq_queue_display_for_time, on_message_display_for_time);
            console.log('connected');
            client_display.subscribe(mq_queue_display_for_compressing, on_message_display_for_compression);
            data = JSON.stringify(form_data);
            client_display.send(mq_queue_display,{},data);
        };

        var on_error_display = function() {
            console.log('error');
        };

        var on_message_display = function(m) {
            console.log(m)
        }

        var on_message_display_for_download = function(m) {
            console.log(m.body);
            message = JSON.parse(m.body);
            if(message["status"] == "download"){
                form_name = message["form-name"];
                persen = message["message"]
                console.log(message)
                var x = document.getElementsByClassName(form_name);
                x[0].style.width = persen+"%"
                x[0].innerHTML = persen+"%"
            }
            else if(message["status"] == "error") {
                var x = document.getElementsByClassName("form-download");
                x[0].removeAttribute("style");
                var y = document.getElementsByClassName("progress-download");
                y[0].style.display = "none";
                document.getElementById("status").innerHTML = "Masukkan 10 URL file yang valid";
                document.getElementById("time-label").innerHTML = "";
                client_display.disconnect();
            }
            else{
                var x = document.getElementsByClassName("progress-download");
                x[0].style.display = "none";
                var y = document.getElementsByClassName("progress-compress");
                y[0].style.display= "";
            }
        };

        var on_message_display_for_time = function(m) {
            //untuk tampilkan waktu
            message = JSON.parse(m.body);
            if(message["status"] == "time-working"){
                document.getElementById("time-label").innerHTML = message["time"]
                document.getElementById("time-label").style.textAlign = "right"
            }
            else{
                client_display.disconnect(function() {
                    console.log("disconect dari websocket");
                  });

            }
        };

        var on_message_display_for_compression = function(m) {
            // untuk tampilan proses kompres
            message = JSON.parse(m.body);
            persen = message["message"]
            var x = document.getElementsByClassName("compress-progress-bar");
            console.log(x);
            x[0].style.width = persen+"%"
            x[0].innerHTML = persen+"%"
            if(message["status"] == "complete"){
                var y = document.getElementById("start-button")
                y.style.display = ""
                y.style.marginTop = "30px"
                y.style.marginLeft = "20px"
            }
            
        };

        client_display.connect('0806444524', '0806444524', on_connect_display, on_error_display, '/0806444524');      
        
        } else {
        // The browser doesn't support WebSocket
        alert("WebSocket NOT supported by your Browser!");
    }
 }

 //sumber:
 // https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
 // 
 function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }


  //sumber:
  // https://stackoverflow.com/questions/11338774/serialize-form-data-to-json
  function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}