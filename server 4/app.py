from flask import Flask, request, send_from_directory
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)

FILES_FOLDER = "File"
if not os.path.exists(FILES_FOLDER):
    os.makedirs(FILES_FOLDER)

# sumber
# https://stackoverflow.com/questions/5870188/does-flask-support-regular-expressions-in-its-url-routing
# https://pythonise.com/series/learning-flask/sending-files-with-flask
@app.route('/download-file/<path:path>', methods=['GET'])
def index(path):
    fileName = path
    return send_from_directory(FILES_FOLDER,fileName,as_attachment=True)

@app.route('/send-file/',methods=['POST'])
def getFile():
    file = request.files["compress-file"]
    if file:
        namaFile = secure_filename(file.filename)
        file.save(os.path.join(FILES_FOLDER, namaFile))
        file.close()
    return j