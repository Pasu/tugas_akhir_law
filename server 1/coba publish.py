#!/usr/bin/env python
import pika
import sys


pika_credential = pika.PlainCredentials("0806444524", "0806444524")
parameters = pika.ConnectionParameters(host ='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=pika_credential)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()

channel.exchange_declare(exchange='1606896174_TOPIC',exchange_type='direct')

message = ' '.join(sys.argv[1:]) or "info: Hello World!"
channel.basic_publish(exchange='1606896174_TOPIC',
                      routing_key='direct.key',
                      body=message)
print(" [x] Sent %r" % (message,))
connection.close()
