from flask import Flask, request
from threading import Thread
import time
import pika
import json


class Time:
    def __init__(self,routing_key):
        self.routing_key = routing_key+".time.key"
        pika_credential = pika.PlainCredentials("0806444524", "0806444524")
        parameters = pika.ConnectionParameters(host ='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=pika_credential)
        self.connection = pika.BlockingConnection(parameters)

        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange='1606896174_TOPIC', exchange_type='topic')
        self.isTimeWorking = True
    
    def startTime(self):
        count = 0
        while (self.isTimeWorking):
            time.sleep(1)
            count += 1
            message = {"time":str(count),"status":"time-working"}
            message = json.dumps(message) 
            self.channel.basic_publish(exchange='1606896174_TOPIC', routing_key = self.routing_key, body=message)
    def stopTime(self):
        self.isTimeWorking = False
        message = {"status":"complete"}
        message = json.dumps(message) 
        self.channel.basic_publish(exchange='1606896174_TOPIC', routing_key = self.routing_key, body=message)
        time.sleep(1)
        self.connection.close()
    def forceStopTime(self):
        self.isTimeWorking = False
        self.connection.close()

app = Flask(__name__)

def async_function():
    timeClass.startTime()



@app.route('/start/', methods=['POST'])
def triggerTimeToStart():
    print("start time")
    routing_key = request.headers['X-ROUTING-KEY']
    global timeClass
    timeClass = Time(routing_key)
    threat = Thread(target=async_function, args=())
    threat.start()


@app.route('/stop/', methods=['POST'])
def triggerTimeToStop():
    print("stop time")
    timeClass.stopTime()

@app.route('/force-stop/',methods=["POST"])
def triggerForceTimeStop():
    print("force stop time")
    timeClass.forceStopTime()