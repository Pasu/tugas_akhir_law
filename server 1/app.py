from flask import Flask, render_template, request, jsonify, flash
import requests

from threading import Thread


app = Flask(__name__)

def async_func(url,headers):
    requests.post(url, headers = headers)


@app.route('/')
def homeindex():
    return render_template('homepage.html')

@app.route('/', methods=['POST'])
def goToServer2():
    routing_key = request.headers['X-ROUTING-KEY']
    headers = {'X-ROUTING-KEY': routing_key}
    urlForServer5 = "http://127.0.0.1:5003/start/"
    requests.post(urlForServer5, headers = headers)
    urlForServer2 = "http://127.0.0.1:5001/"
    thread1 = Thread(target=async_func, args=(urlForServer2,headers))
    thread1.start()
    return jsonify(status="Upload ke server 2")
