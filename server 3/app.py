from flask import Flask, request,jsonify
from threading import Thread

import pika
import time
import os
import json
import zipfile
import requests
import base64
import hashlib
import calendar
import datetime
from werkzeug.utils import secure_filename

FILES_FOLDER = 'File'
FILE_ZIP_NAME = "compress_file"
COMPRESS_FOLDER = 'Compress'
urlServer4 = "http://infralabs.cs.ui.ac.id:20688/"
urlNginx = "http://infralabs.cs.ui.ac.id:20689/"

if not os.path.exists(FILES_FOLDER):
    os.makedirs(FILES_FOLDER)

if not os.path.exists(COMPRESS_FOLDER):
    os.makedirs(COMPRESS_FOLDER)

app = Flask(__name__)

# sumber
# https://stackoverflow.com/questions/11612032/how-to-generate-a-nginx-secure-link-in-python
def checkToServer4():
    fileZIpName = FILE_ZIP_NAME+".zip"
    files = {"compress-file":open(os.path.join(COMPRESS_FOLDER,fileZIpName),"rb")}
    requests.post(urlServer4+"send-file/",files=files)
    url = "/secure/"+fileZIpName
    
    secret = "itsaSSEEECRET"

    future = datetime.datetime.utcnow() + datetime.timedelta(minutes=5)
    expiry = calendar.timegm(future.timetuple())

    secure_link = f"{secret}{url}{expiry}".encode('utf-8')

    hash = hashlib.md5(secure_link).digest()
    base64_hash = base64.urlsafe_b64encode(hash)
    str_hash = base64_hash.decode('utf-8').rstrip('=')

    print(f"{url}?st={str_hash}&e={expiry}")

def zip_file(namaZIP, tempatFile):
    zf = zipfile.ZipFile(os.path.join(COMPRESS_FOLDER, namaZIP + ".zip"), "w", zipfile.ZIP_DEFLATED)
    list_file = os.listdir(tempatFile)
    for i in list_file:
        zf.write(os.path.join(tempatFile,i))

def startZIP(namaZIP, tempatFile, route_key):
    time.sleep(2)

    pika_credential = pika.PlainCredentials("0806444524", "0806444524")
    parameters = pika.ConnectionParameters(host ='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=pika_credential)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()

    channel.exchange_declare(exchange='1606896174_TOPIC', exchange_type='topic')

    thread = Thread(target = zip_file, args=(namaZIP, tempatFile,))
    thread.start()
    current_progress = 0

    while thread.is_alive():
        try:
            zip_progress = os.path.getsize(os.path.join(COMPRESS_FOLDER,namaZIP + ".zip")) / os.path.getsize(os.path.join(tempatFile)) * 100
            if(zip_progress>100):
                continue
            elif zip_progress > current_progress:
                current_progress += 10
                message = {"status":"compress","message":str(current_progress)}
                message = json.dumps(message) 
                channel.basic_publish(exchange='1606896174_TOPIC', routing_key = route_key, body=message)
        except Exception:
            pass 
    message = {"status":"complete","message":"100"}
    message = json.dumps(message) 
    channel.basic_publish(exchange='1606896174_TOPIC', routing_key = route_key, body=message)

    connection.close()
    print("masuk stop")
    urlForServer5 = "http://127.0.0.1:5003/stop/"
    requests.post(urlForServer5)
    list_file = os.listdir(FILES_FOLDER)
    for i in list_file:
        os.remove(os.path.join(FILES_FOLDER,i))
    
    checkToServer4()



@app.route('/', methods=['POST'])
def compress():
    for i in range(1,11):
        formName = "url-"+str(i)
        file = request.files[formName]
        if file:
            namaFile = secure_filename(file.filename)
            file.save(os.path.join(FILES_FOLDER, namaFile))
            request.files[formName].close()
    namaZIP = FILE_ZIP_NAME
    route_key = request.headers['X-ROUTING-KEY']
    compress_key = route_key+".compressing.key"
    startZIP(namaZIP,FILES_FOLDER,compress_key)
    return jsonify(status="status")
    