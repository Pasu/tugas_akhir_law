from flask import Flask, request

from threading import Thread
import pika
import time
import wget
import os
import requests
import validators
import json
from urllib.request import urlopen

app = Flask(__name__)

FILES_FOLDER = 'File'

if not os.path.exists(FILES_FOLDER):
    os.makedirs(FILES_FOLDER)

#sumber: 
# https://www.bogotobogo.com/python/RabbitMQ_Celery/python_Publish_Subscribe_Exchanges_RabbitMQ_Celery.php
# https://github.com/pika/pika/issues/919
# https://github.com/rabbitmq/rabbitmq-website/issues/780


def downloadUrl(listUrl,listUrlFormName,route_key):
    listFileSize = []
    print("masuk download url")
    for i in listUrl:
        size = getFileSize(i)
        if(size != None):
            listFileSize.append(size)
        else:
            return False
    thread = Thread(target=async_func, args=(listUrl,listUrlFormName, listFileSize, route_key))
    thread.start()
    return True


def async_func(listUrl,listUrlFormName, listFileSize, route_key):
    print(route_key)
    pika_credential1 = pika.PlainCredentials("0806444524", "0806444524")
    parameters1 = pika.ConnectionParameters(host ='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=pika_credential1)
    connection1 = pika.BlockingConnection(parameters1)

    channel1 = connection1.channel()

    downloadKey = route_key+".download.key"

    channel1.exchange_declare(exchange='1606896174_TOPIC', exchange_type='topic')
    if(len(listUrl)==10):
        for i in range(10):
            download = Download(listUrl[i],listUrlFormName[i], listFileSize[i], downloadKey,channel1)
            download.startDownloadUrl()
        message = {"status":"compress"}
        message = json.dumps(message) 
        channel1.basic_publish(exchange='1606896174_TOPIC', routing_key = downloadKey, body=message)
        connection1.close()
        time.sleep(1)
        url = "http://127.0.0.1:5002/"
        headers = {'X-ROUTING-KEY': route_key}
        files = {}
        listFile = os.listdir(FILES_FOLDER)
        for i in range(1,len(listFile)+1):
            formName = "url-"+str(i)
            files[formName] = open(os.path.join(FILES_FOLDER, listFile[i-1]),"rb")
            
        requests.post(url,files=files, headers = headers)
        for i in range(1,len(listFile)+1):
            formName = "url-"+str(i)
            files[formName].close()
        
        list_file = os.listdir(FILES_FOLDER)
        for i in list_file:
            os.remove(os.path.join(FILES_FOLDER,i))
        print("masuk server 3")
    else:
        message = {"status":"error"}
        message = json.dumps(message) 
        channel1.basic_publish(exchange='1606896174_TOPIC', routing_key = downloadKey, body=message)
        connection1.close()
        print("masuk stop")
        urlForServer5 = "http://127.0.0.1:5003/force-stop/"
        requests.post(urlForServer5)

# sumber:
# https://gist.github.com/kissgyorgy/6102803
# https://stackoverflow.com/questions/2792650/import-error-no-module-name-urllib2
def getFileSize(url):
    try:
        u = urlopen(url)
        meta = u.info()
        file_size = int(meta.get("Content-Length"))
        return file_size
    except:
        return None

class Download:
    def __init__(self,url,formName,fileSize,route_key,channel):
        self.url = url
        self.formName = formName
        self.route_key = route_key
        self.fileSize = fileSize
        self.channel = channel

    def getFileName(self):
        fileName = self.url.split("/")[-1].split("?")[0]
        listFile = os.listdir(FILES_FOLDER)
        if fileName in listFile:
            name = fileName.split(".")[0]
            count = 1
            for i in listFile:
                if name in i:
                    count += 1
            name = name+"_"+str(count)
            try:
                extension = fileName.split(".")[1]
                name = name+"."+extension
                return name
            except:
                return name
        else:
            return fileName

    # sumber:
    # https://gist.github.com/kissgyorgy/6102803
    # https://stackoverflow.com/questions/2792650/import-error-no-module-name-urllib2

    def startDownloadUrl(self):
        time.sleep(1)
        
        
        
        fileName = self.getFileName()
        print(fileName)
        u = urlopen(self.url)
        f = open(os.path.join(FILES_FOLDER,fileName), 'wb')
        block_sz = 8000
        count = 0
        persentage = 0
        while True:
            buffer = u.read(block_sz)
            if not buffer:
                break
            count += len(buffer)
            persen = (count/self.fileSize)*100
            if(persen>persentage):
                persentage += 10
                message = {"message":str(persentage),"form-name":self.formName,"status":"download"}
                message = json.dumps(message) 
                self.channel.basic_publish(exchange='1606896174_TOPIC', routing_key = self.route_key, body=message)
            f.write(buffer)
        f.close()
        #wget.download(url, os.path.join(FILES_FOLDER,fileName))
        message = {"message":"100","form-name":self.formName,"status":"download"}
        message = json.dumps(message) 
        self.channel.basic_publish(exchange='1606896174_TOPIC', routing_key = self.route_key, body=message)
    
            




@app.route('/', methods=['POST'])
def download():
    print("hehehe")
    routing_key = request.headers['X-ROUTING-KEY']
    direct_routing_key = routing_key+".direct.key"

    pika_credential = pika.PlainCredentials("0806444524", "0806444524")
    parameters = pika.ConnectionParameters(host ='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=pika_credential)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    channel.exchange_declare(exchange='1606896174_DIRECT',exchange_type='direct')

    result = channel.queue_declare(queue='',exclusive=True)
    queue_name = result.method.queue

    channel.queue_bind(exchange='1606896174_DIRECT',queue=queue_name,routing_key=direct_routing_key)

    print(' [*] Waiting for logs. To exit press CTRL+C')

    def callback(ch, method, properties, body):
        jsonUrl = json.loads(body)
        print("start download")
        listUrlValid = []
        listUrlFormName = []
        for j in range(1,11):
            i = "url-"+str(j)
            url = jsonUrl[i]
            if url != "":
                urlValid = validators.url(url)
                if(urlValid):
                    listUrlValid.append(url)
                    listUrlFormName.append(i)
        downloadUrl(listUrlValid,listUrlFormName,routing_key)
        connection.close()

    channel.basic_consume(on_message_callback=callback,queue=queue_name,auto_ack=True)
    channel.start_consuming()